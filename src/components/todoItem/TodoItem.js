import React from 'react'
import { v4 as listID } from 'uuid';


class TodoItem extends React.Component{
  render(){
    return (
      <li className={this.props.completed ? "completed" : ""} key ={listID()}>
        <div className="view">
          <input 
            className="toggle" 
            type="checkbox" 
            onChange = {() => this.props.checkTodo(this.props.id)}            
            defaultChecked={this.props.completed}
            />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={() => this.props.deleteTodo(this.props.id)}/>
        </div>
      </li>
    );}
  
}

export default TodoItem