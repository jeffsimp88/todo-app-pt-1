import React from 'react'
import TodoItem from './../todoItem/TodoItem'

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
              checkTodo ={this.props.checkTodo}
              deleteTodo ={this.props.deleteTodo}
              title={todo.title}
              completed={todo.completed}
              id={todo.id}
            />
          ))}
        </ul>
      </section>
    );
  }
}


export default TodoList