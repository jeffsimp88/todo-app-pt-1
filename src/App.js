import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as listID } from 'uuid';

//Recieved assistance from Kano & Ryland

class App extends Component {
  state = {
    todos: todosList,
    formData: { input: "" },
  };

  handleChange = (event) => {
    const formData = { ...this.state.formData };
    formData["input"] = event.target.value;
    this.setState({ formData });
  };

  checkTodo = (checkID) => {
    const newTodos = this.state.todos.map(
      (todoItem) => {
      if (todoItem.id === checkID) {
        todoItem.completed = !todoItem.completed;
      }
      });
    this.setState({ newTodos });
  };

  deleteTodo = (todoID) => {
    const newTodos = this.state.todos.filter(
      (todoItem) => todoItem.id !== todoID
    );
    this.setState({ todos: newTodos });
  };

  deleteComplete = () => {
    const newTodos = this.state.todos.filter(
      (todoItem) => todoItem.completed === false
    );
    this.setState({ todos: newTodos });
  };

  addTodo = (event) => {
    if (event.key === "Enter") {
      const newItem = this.state.formData.input;
      if (newItem !== "")
        this.state.todos.push({
          userId: 1,
          id: listID(),
          title: newItem,
          completed: false,
        });
      event.target.value = "";
    }
    this.setState(this.state.todos);
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            type="text"
            value={this.state.value}
            className="new-todo"
            onChange={this.handleChange}
            onKeyDown={this.addTodo}
            placeholder="What needs to be done?"
            autoFocus
          />
        </header>
        <TodoList
          todos={this.state.todos}
          checkTodo={this.checkTodo}
          deleteTodo={this.deleteTodo}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>{this.state.todos.length}</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.deleteComplete}>
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}


class TodoItem extends Component{
  render(){
    return (
      <li className={this.props.completed ? "completed" : ""} key ={listID()}>
        <div className="view">
          <input 
            className="toggle" 
            type="checkbox" 
            onChange = {() => this.props.checkTodo(this.props.id)}            
            defaultChecked={this.props.completed}
            />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={() => this.props.deleteTodo(this.props.id)}/>
        </div>
      </li>
    );}
  
}


class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
              checkTodo ={this.props.checkTodo}
              deleteTodo ={this.props.deleteTodo}
              title={todo.title}
              completed={todo.completed}
              id={todo.id}
            />
          ))}
        </ul>
      </section>
    );
  }
}



export default App;
